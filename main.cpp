#include <stdlib.h>
#include <iostream>
#include <json/json.h>
#include <stdio.h>

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#include "src/DB/DBConnector.h"
#include "src/Repository/UserRepository.h"
#include "src/Handler/UserHandler.h"

using namespace std;
using namespace sql;

json_object* handleGetUsers(vector<User> users) {
    json_object *usersJSON = json_object_new_array();

    for(int i = 0; i < users.size(); i++) {
        json_object *userJSON = json_object_new_object();

        json_object *userID = json_object_new_int(users[i].getId());
        json_object *userFirstName = json_object_new_string(users[i].getFirstName().c_str());
        json_object *userLastName = json_object_new_string(users[i].getLastName().c_str());

        json_object_object_add(userJSON, "id", userID);
        json_object_object_add(userJSON, "first_name", userFirstName);
        json_object_object_add(userJSON, "last_name", userLastName);

        json_object_array_add(usersJSON, userJSON);
    }

    return usersJSON;
}

int main(void) {
    try {
        // Create a new dbConnector for the database test, user root and password root
        DBConnector dbConnector = DBConnector("test", "root", "root");
        dbConnector.init();

        Connection *con = dbConnector.getConnection();
        UserRepository repo = UserRepository(con, dbConnector);
        UserHandler handler = UserHandler(&repo, &dbConnector);

        vector<User> results;

        // Simulate API Call for path /api/users
        results = handler.GetAll();
        auto usersJSON = handleGetUsers(results);

        // Return the json
        printf("The json created: %s\n", json_object_to_json_string(usersJSON));

        delete con;
    } catch (sql::SQLException &e) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line "
        << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() <<
        " )" << endl;
    }

    cout << endl;

    return EXIT_SUCCESS;
}
