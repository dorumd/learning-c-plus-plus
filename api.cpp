// HTTP Server Deps
#include "lib/server_http.hpp"
#define BOOST_SPIRIT_THREADSAFE
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <fstream>
#include <boost/filesystem.hpp>
#include <vector>
#include <algorithm>

// Mysql DEPS
#include <stdlib.h>
#include <iostream>
#include <json/json.h>
#include <stdio.h>
#include <string>

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#include "src/DB/DBConnector.h"
#include "src/Repository/UserRepository.h"
#include "src/Handler/UserHandler.h"

using namespace std;
using namespace sql;
using namespace boost::property_tree;

typedef SimpleWeb::Server<SimpleWeb::HTTP> HttpServer;

json_object* handleGetUsers(vector<User> users) {
    json_object *usersJSON = json_object_new_array();

    for(int i = 0; i < users.size(); i++) {
        json_object *userJSON = json_object_new_object();

        json_object *userID = json_object_new_int(users[i].getId());
        json_object *userFirstName = json_object_new_string(users[i].getFirstName().c_str());
        json_object *userLastName = json_object_new_string(users[i].getLastName().c_str());

        json_object_object_add(userJSON, "id", userID);
        json_object_object_add(userJSON, "first_name", userFirstName);
        json_object_object_add(userJSON, "last_name", userLastName);

        json_object_array_add(usersJSON, userJSON);
    }

    return usersJSON;
}

int main() {
    HttpServer server(8080, 1);

    server.resource["^/api/users$"]["GET"] = [](HttpServer::Response& response, shared_ptr<HttpServer::Request> request) {
        try {
            DBConnector dbConnector = DBConnector("test", "root", "root");
            dbConnector.init();

            Connection *con = dbConnector.getConnection();
            UserRepository repo = UserRepository(con, dbConnector);
            UserHandler handler = UserHandler(&repo, &dbConnector);

            vector<User> results;
            results = handler.GetAll();
            auto usersJSON = handleGetUsers(results);
            string jsonResponse = json_object_to_json_string(usersJSON);

            delete con;

            response << "HTTP/1.1 200 OK\r\nContent-Length: " << jsonResponse.length() << "\r\n\r\n" << jsonResponse;
        } catch (sql::SQLException &e) {
            cout << "# ERR: SQLException in " << __FILE__;
            cout << "(" << __FUNCTION__ << ") on line "
            << __LINE__ << endl;
            cout << "# ERR: " << e.what();
            cout << " (MySQL error code: " << e.getErrorCode();
            cout << ", SQLState: " << e.getSQLState() <<
            " )" << endl;
        }
    };

    thread server_thread([&server](){
        server.start();
    });

    server_thread.join();

    return 0;
}