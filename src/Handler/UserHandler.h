#ifndef MYSQL_EXAMPLE_USERHANDLER_H
#define MYSQL_EXAMPLE_USERHANDLER_H

#include "../Repository/UserRepository.h"
#include "../DB/DBConnector.h"
#include "../Model/User.h"

class UserHandler {
private:
    UserRepository* repository;
    DBConnector* connector;
public:
    UserHandler(UserRepository* repo, DBConnector* conn);
    User Get(int id);
    vector<User> GetAll();
};


#endif //MYSQL_EXAMPLE_USERHANDLER_H
