#include "UserHandler.h"

UserHandler::UserHandler(UserRepository* repo, DBConnector* conn){
    repository = repo;
    connector = conn;
}

User UserHandler::Get(int id) {
    User user;
    user = repository->findOneById(id);

    return user;
}

vector<User> UserHandler::GetAll() {
    vector<User> users;
    users = repository->findAll();

    return users;
}
