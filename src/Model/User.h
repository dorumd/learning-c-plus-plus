#ifndef MYSQL_EXAMPLE_USER_H
#define MYSQL_EXAMPLE_USER_H


#include <string>

using namespace std;

class User {
private:
    string firstName;
    string lastName;
    int id;
public:
    User();

    string getFirstName() const;

    void setFirstName(string firstName = "");

    string getLastName() const;

    void setLastName(string lastName = "");

    int getId() const;

    void setId(int id = -1);
};


#endif