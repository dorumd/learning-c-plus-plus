#include "User.h"

User::User() { }

string User::getFirstName() const {
    return this->firstName;
}

void User::setFirstName(string firstName) {
    this->firstName = firstName;
}

string User::getLastName() const {
    return this->lastName;
}

void User::setLastName(string lastName) {
    this->lastName = lastName;
}

int User::getId() const {
    return this->id;
}

void User::setId(int id) {
    this->id = id;
}
