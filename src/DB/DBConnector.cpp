#include "DBConnector.h"

using namespace sql;

DBConnector::DBConnector(string db, string u, string p) {
    database = db;
    user = u;
    password = p;
}

void DBConnector::init() {
    Driver *driver;
    driver = get_driver_instance();

    this->connection = driver->connect("tcp://127.0.0.1:3306", this->user, this->password);
    this->connection->setSchema(this->database);
}

Connection *DBConnector::getConnection() {
    return this->connection;
}

void DBConnector::initTables() {
    sql::Statement *stmt;

    try {
        stmt = this->connection->createStatement();
        stmt->execute("DROP TABLE IF EXISTS users");
        stmt->execute("CREATE TABLE users(id INT, gender int, first_name varchar(50), last_name varchar(50))");

        delete stmt;
    } catch (SQLException &e) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line "
        << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() <<
        " )" << endl;
    }
}

ResultSet *DBConnector::executeStatement(PreparedStatement *preparedStatement) {
    ResultSet *resultSet;

    try {
        resultSet = preparedStatement->executeQuery();
        delete preparedStatement;
    } catch (sql::SQLException &e) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line "
        << __LINE__ << endl;
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() <<
        " )" << endl;
    }

    return resultSet;
}
