#ifndef MYSQL_EXAMPLE_DBCONNECTOR_H
#define MYSQL_EXAMPLE_DBCONNECTOR_H


#include <string>
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

using namespace std;
using namespace sql;

class DBConnector {
private:
    Connection *connection;
    string database;
    string user;
    string password;
public:
    DBConnector(string database = "test", string user = "root", string password = "");

    void init();

    void initTables();

    Connection *getConnection();

    ResultSet* executeStatement(PreparedStatement *statement);
};


#endif //MYSQL_EXAMPLE_DBCONNECTOR_H
