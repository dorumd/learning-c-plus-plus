#include "UserRepository.h"

using namespace sql;

UserRepository::UserRepository(Connection *con, DBConnector conn) {
    connection = con;
    connector = conn;
}

std::vector<User> UserRepository::findAll() {
    vector<User> users;
    string statement = "SELECT * FROM users ORDER BY id DESC";

    ResultSet *res;
    PreparedStatement *pstmt;

    pstmt = this->connection->prepareStatement(statement);

    res = connector.executeStatement(pstmt);

    while (res->next()) {
        User user = User();
        user.setFirstName(res->getString("first_name"));
        user.setLastName(res->getString("last_name"));
        user.setId(res->getInt("id"));
        cout << "\t... User found: " << user.getId() << " " << user.getFirstName() << endl;
        users.push_back(user);
    }

    delete res;

    return users;
}

User UserRepository::findOneById(int id) {
    User result;

    string statement = "SELECT * FROM users WHERE id = (?) ORDER BY id DESC LIMIT 1";

    ResultSet *res;
    PreparedStatement *pstmt;

    pstmt = this->connection->prepareStatement(statement);
    pstmt->setInt(1, id);

    res = connector.executeStatement(pstmt);

    while (res->next()) {
        result.setFirstName(res->getString("first_name"));
        result.setLastName(res->getString("last_name"));
        result.setId(res->getInt("id"));
        cout << "\t... User found: " << result.getId() << " " << result.getFirstName() << endl;
    }

    delete res;

    return result;
}
