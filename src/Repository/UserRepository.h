#ifndef MYSQL_EXAMPLE_USERREPOSITORY_H
#define MYSQL_EXAMPLE_USERREPOSITORY_H


#include <string>

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include "../Model/User.h"
#include <array>

#include "../DB/DBConnector.h"

using namespace std;
using namespace sql;

class UserRepository {
private:
    Connection *connection;
    DBConnector connector;
public:
    UserRepository(Connection *connection, DBConnector connector);

    vector<User> findAll();

    User findOneById(int id);
};


#endif
